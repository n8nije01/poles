import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  private height: number;
  private factor: any;
  private result: number;

  constructor() {}

  ngOnInit() {
    this.factor = '0.82';
    this.result = 0;
    this.height = 100;
  }

  private calculate() {
    this.result = this.height * parseFloat(this.factor);
  }

}
